var express = require("express");
var app = express();

// Static resources will be served from here
app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.error("Application is listening on port 3000");
});